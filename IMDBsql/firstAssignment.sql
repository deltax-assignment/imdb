use db;
create table classes(name varchar(32),section varchar(32),number int);
create table teachers(name varchar(32),dob date,gender varchar(32));
create table students(name varchar(32),dob date,gender varchar(32),classid int);
INSERT INTO classes values ('IX','A',201)
INSERT INTO classes values ('IX','B',202)
INSERT INTO classes values ('X','A',203)

INSERT INTO teachers values ('Lisa Kudrow','1985/06/08','Female')
INSERT INTO teachers values ('Monica Bing','1982/03/06','Female')
INSERT INTO teachers values ('Chandler Bing','1978/12/17','Male')
INSERT INTO teachers values ('Ross Geller','1993/01/26','Male')

INSERT INTO students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO students values ('Dave Grohl','2005/02/12','Male',3)
create table teacherClassMapping(teacherid int,classid int);
INSERT INTO teacherClassMapping values (1,1)
INSERT INTO teacherClassMapping values (1,2)
INSERT INTO teacherClassMapping values (2,2)
INSERT INTO teacherClassMapping values (2,3)
INSERT INTO teacherClassMapping values (3,3)
INSERT INTO teacherClassMapping values (3,1)
use db;
select * from students;
select * from students where gender = 'Male';
select * from students where dob > '2005/01/01';
SELECT * FROM students WHERE dob = (select MAX(dob) FROM students); 
select DISTINCT dob from students ;
select count(*) from students,classes where classid = number group by classes.number;

SELECT classid, COUNT(*) as Count FROM Students GROUP BY classid;
SELECT C.section, COUNT(*) AS Count from Classes C JOIN Students S ON C.number = S.classid GROUP BY C.section;
alter table students add id int not null identity (1,1);
alter table students add constraint id primary key(id);
select * from students;
alter table teachers add id int not null identity (1,1);
alter table teachers add constraint tid primary key(id);
alter table classes add id int not null identity (1,1);
alter table classes add constraint cid primary key(id);

SELECT  T.id,T.name , COUNT(*) AS Count FROM Teachers T JOIN TeacherClassMapping TCM ON T.id = TCM.TeacherId GROUP BY T.Name,T.id;
SELECT T.name , C.name, C.section FROM Teachers T JOIN TeacherClassMapping TCM ON T.Id = TCM.TeacherId JOIN Classes C ON C.Id = TCM.ClassId WHERE C.Name = 'ix'
select c.name,c.section,p.name from classes c join (select t.name,r.classid from  teachers t join ( select q.classid,tcm.teacherid from teacherClassMapping tcm join(SELECT classid FROM TeacherClassMapping GROUP BY classid HAVING COUNT(*) >= 2) q on tcm.classid = q.classid) r on t.id = r.teacherid) p on p.classid = c.id ;
SELECT S.* FROM students S  JOIN classes C ON C.id = S.classid JOIN teacherclassmapping TCM ON TCM.classid = C.id INNER JOIN teachers T ON T.Id = TCM.teacherid WHERE T.Name LIKE '%Lisa%'