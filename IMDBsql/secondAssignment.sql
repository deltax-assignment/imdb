use assignment

--actors table
create table actors 
(id int identity(1,1),
name varchar(32),
gender varchar(32),
dateofbirth date,
primary key (id));

--companies table
create table companies
(id int identity(1,1),
name varchar(32),
estdate date,
primary key(id));

--producers table
create table producers
(id int identity(1,1),
name varchar(32),
company_id int,
primary key(id),
foreign key (company_id) references companies(id));


--movies table
create table movies
(id int identity(1,1),
name varchar(36),
producer_id int,
language varchar(38),
profit int,
primary key (id),
foreign key (producer_id) references producers(id));

--actor movie mapping table 
create table actor_movie_mapping
(id int identity(1,1),
movieid int,
actorid int,
primary key(id),
foreign key(movieid) references movies(id),
foreign key(actorid) references actors(id));


 insert into actors (name,gender,dateofbirth) values
 ('Mila Kunis',
'Female',
'11/14/1986'),
('Robert DeNiro',
'Male',
'07/10/1957'),
('George Michael',
'Male',
'11/23/1978'),
('Mike Scott',
'Male',
'08/06/1969'),
('Pam Halpert',
'Female',
'09/26/1996'),
('Dame Judi Dench',
'Female',
'04/05/1947');

insert into companies(name,estdate)values
('fox','05/14/1998'),
('bull','09/11/2004'),
('hanks','11/03/1987'),
('male','11/14/1996'),
('team coco','09/26/1992');

insert into producers ( name,company_id) values
('arjun',1),
('arun',2),
('tom',3),
('zeshan',4),
('nicole',5);

insert into movies (name,language,producer_id,profit) values
('Rocky','English',1,10000),
('Rocky','Hindi',3,3000),
('Terminal','English',4,300000),
('Rambo','Hindi',2,93000),
('Rudy','English',5,9600)

insert into actor_movie_mapping (movieid,actorid) values
(1,1),(1,3),(1,5),
(2,6),(2,5),(2,4),(2,2),
(3,3),(3,2),
(4,1),(4,6),(4,3),
(5,2),(5,5),(5,3)


--Update Profit of all the movies by +1000 where producer name contains 'run'
--1st way
 update movies 
 set profit = profit + 1000 
 where id = (select m.id from movies m where m.producer_id = (select p.id from producers p where p.name like'%run%'));
 --2nd way
 update movies 
 set profit = profit + 1000 
 where producer_id = (select p.id from producers p where p.name like'%run%');
 --3rd way
 update movies 
 set profit = profit +1000  
 from movies m join producers p on m.producer_id = p.id  where p.name like'%run%' 


--Find duplicate movies having the same name and their count

 select name,count(name) count from movies group by name having count(name)>1;

--Find the oldest actor/actress for each movie

 select r.name,a.name,r.oldest 
 from (select m.id,m.name,
 min(a.dateofbirth) as oldest 
 from movies m join actor_movie_mapping amm 
 on m.id = amm.movieid 
 join actors a 
 on a.id = amm.actorid 
 group by m.id,m.name ) r 
 join actor_movie_mapping amm 
 on r.id = amm.movieid 
 join actors a 
 on a.id = amm.actorid  
 where a.dateofbirth = r.oldest

--Add non-clustered index on profit column of movies table
 
 create nonclustered index IX_profit on movies(profit)

--Create stored procedure to return list of actors for given movie id

 create procedure usp_GetActorsById @id int as begin
 select a.name from actors a 
 join actor_movie_mapping amm 
 on a.id = amm.actorid 
 where amm.movieid = @id
 end
 exec usp_GetActorsById @id = 1

--Create a function to return age for given date of birth
 create function fn_getAge( @dob date)
returns int
as begin return (select DATEDIFF(year,@dob,CURRENT_TIMESTAMP))
end
select dbo.fn_getAge('09/09/2000')

--Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) 

 create procedure usp_IncrementProfit @ids varchar(32) as begin
 update movies set profit = profit + 100 where id in (select * from string_split(@ids,',')) end
 exec usp_IncrementProfit @ids = '1,2,3,4'
 select * from movies;

--List of producers who have not worked with actor 'mike scott'
  --1st way
  select * from producers ps 
  where ps.id not in 
  (select p.id from movies m 
  join actor_movie_mapping amm 
  on m.id = amm.movieid 
  join (select * 
  from actors a 
  where a.name 
  like'mike scott') r 
  on r.id = amm.actorid 
  join producers p 
  on p.id = m.producer_id)
 --2nd way
 select * from producers ps 
   left join 
  (select p.id from movies m 
  join actor_movie_mapping amm 
  on m.id = amm.movieid 
  join (select * 
  from actors a 
  where a.name 
  like'mike scott') r 
  on r.id = amm.actorid 
  join producers p 
  on p.id = m.producer_id) q 
  on ps.id = q.id where q.id is null 


--List of pair of actors who have worked together in more than 2 movies

 select amm1.actorid,amm2.actorid ,count(*)
from actors a1 join actor_movie_mapping amm1 
on a1.id = amm1.actorid,actors a2 join actor_movie_mapping amm2 
on a2.id = amm2.actorid
where a1.id < a2.id and amm1.movieid = amm2.movieid 
group by amm1.actorid,amm2.actorid 
having count(*)=2; 