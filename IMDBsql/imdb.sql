--actors
create table actors
(id int identity(1,1) primary key,
name varchar(32),
gender varchar(32),
dateofbirth date,
bio varchar (100));

--producers
create table producers 
(id int identity(1,1) primary key,
name varchar(32),
gender varchar(32),
dateofbirth date,
bio varchar(100));

--movies 
create table movies
(id int identity(1,1) primary key,
name varchar(32),
yearofrelease date,
producerid int,
plot varchar(32),
poster image,
foreign key (producerid)references producers(id));

--actor movie mapping
create table actor_movie_mapping
(id int identity(1,1) primary key ,
movieid int,
actorid int,
foreign key(movieid) references movies(id),
foreign key(actorid) references actors(id));


--genre
create table genre
(id int identity(1,1) primary key,
name varchar(32));

--genre movie mapping
create table genre_movie_mapping 
(id int identity(1,1) primary key,
movieid int,
genreid int,
foreign key(movieid) references movies(id),
foreign key(genreid) references genre(id));



