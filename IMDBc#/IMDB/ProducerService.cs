﻿using IMDB.Domain;
using IMDB.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace IMDB
{

    public class ProducerService
    {
        private readonly ProducerRepo _producerRepo;

        public ProducerService()
        {
            _producerRepo = new ProducerRepo();
        }
        public void AddProducer(string name, string birth)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Invalid arguments");
            }
            DateTime pBirth;
            if (DateTime.TryParseExact(birth.Trim(), "dd/MM/yyyy", null, DateTimeStyles.None, out DateTime dateTime))
            {
                pBirth = dateTime;//    ToString("dd/MM/yyyy");
            }
            else
            {
                throw new Exception("not valid date");
            }
            var person = new Producer()
            {
                Name = name,
                DateOfBirth = pBirth,
                Id = GetAll().Count + 1
            };
            _producerRepo.Add(person);
        }
        public List<Producer> GetAll()
        {
            return _producerRepo.GetProducer();
        }

        public Producer GetEntity(int producerId)
        {
            if (producerId <= 0)
            {
                throw new ArgumentNullException(nameof(producerId));
            }
            var producer = GetAll().SingleOrDefault(p => p.Id == producerId);
            
            if (producer == null)
            {
                throw new Exception($"producer not found for id {producerId}");
            }
            return producer;
        }

        public void DeleteProducer(string id)
        {
            int producerId;
            if (int.TryParse(id, out int parsedProducerId) && parsedProducerId > 0)
            {
                producerId = parsedProducerId;
            }
            else
            {
                throw new Exception("producer id invalid");
            }
            Producer producer = GetEntity(producerId);
            _producerRepo.DeleteProducer(producer);
        }
    }
}
