﻿using IMDB.Domain;
using IMDB.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace IMDB
{
    public class MovieService
    {
        private readonly MovieRepo _imdbRepository;
        private readonly ProducerService _producerService;
        private readonly ActorService _actorService;

        public MovieService(ActorService actorService, ProducerService producerService)
        {
            _imdbRepository = new MovieRepo();
            _producerService = producerService;
            _actorService = actorService;
        }

        public int AddMovie(string name, string releaseDate, string plot, string actorIds, string producerIdStr)
        {
            int release = 0;
            if (int.TryParse(releaseDate, out int parsedReleasedDate) && parsedReleasedDate > 0)
            {
                release = parsedReleasedDate;
            }
            else
            {
                throw new Exception("enter valid choice");
            }

            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(plot))
            {
                throw new Exception("Invalid arguments");
            }
            if (string.IsNullOrEmpty(actorIds))
            {
                throw new Exception("Actor id cannot be empty");
            }
            if (string.IsNullOrEmpty(producerIdStr))
            {
                throw new Exception("producer id cannot be empty");
            }
            var parsedActorIds = new List<int>();
            foreach (var id in actorIds.Split(','))
            {
                if (int.TryParse(id, out int parseActorId) && parseActorId > 0)
                {
                    parsedActorIds.Add(parseActorId);
                }
                else
                {
                    throw new Exception($"Actor Id not valid {parseActorId}");
                }
            }
            int producerId = 0;
            if (int.TryParse(producerIdStr, out int parseProducerId) && parseProducerId > 0)
            {
                producerId = parseProducerId;
            }
            else
            {
                throw new ArgumentException("Invalid Input");
            }
            var producer = _producerService.GetEntity(producerId);
            var newActor = new List<Actor>();
            foreach (var parseActorId in parsedActorIds)
            {
                newActor.Add(_actorService.GetEntity(parseActorId));
            }
            var movie = new Movie()
            {
                Title = name,
                YearOfRelease = release,
                Plot = plot,
                Actors = newActor,
                Producer = producer,
                Id = GetAll().Count + 1
            };
            _imdbRepository.AddMovie(movie);
            return movie.Id;
        }

        public List<Movie> GetAll()
        {
            return _imdbRepository.Get();
        }


        public void Delete(string Id)
        {

            if (int.TryParse(Id, out int parsedMovieId) && parsedMovieId > 0)
            {

                _imdbRepository.DeleteMovie(GetEntity(parsedMovieId));
            }
            else
            {
                throw new Exception("Movie id not valid");
            }


        }


        public Movie GetEntity(int movieId)
        {
            if (movieId <= 0)
            {
                throw new ArgumentNullException(nameof(movieId));
            }
            var movie = GetAll().SingleOrDefault(m => m.Id == movieId);
            if (movie == null)
            {
                throw new Exception("Movie not found");
            }
            return movie;
        }


        public void Display()
        {
            var movies = GetAll();
            if (movies.Count == 0)
            {
                Console.WriteLine("Please add movie first  \n");
                return;
            }
            Console.WriteLine("Movie List");
            foreach (var movie in movies)
            {
                Console.WriteLine("Name of Movie is " + movie.Title);
                Console.WriteLine("Year of release is " + movie.YearOfRelease);
                Console.WriteLine("Plot is " + movie.Plot);
                Console.WriteLine("Producer is " + movie.Producer.Name);
                Console.WriteLine("Actor is ");

                foreach (var actor in movie.Actors)
                {
                    Console.WriteLine(actor.Id + " " + actor.Name);
                }
                Console.WriteLine("\n");
            }
        }


    }
}

