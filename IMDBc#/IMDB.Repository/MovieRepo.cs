﻿using System;
using System.Linq;
using IMDB.Domain;
using System.Collections.Generic;

namespace IMDB.Repository
{
    
    public class MovieRepo
    {

        private readonly List<Movie> _movies;

        public MovieRepo()
        {
            _movies = new List<Movie>();
        }
        public void AddMovie(Movie movie)
        {
            _movies.Add(movie);
        }

        public void DeleteMovie(Movie movie)
        {
            _movies.Remove(movie);
        }

        public List<Movie> Get()
        {
            return _movies.ToList();
        }

        
    }
}
