﻿using IMDB.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMDB.Repository
{
    public class ProducerRepo
    {
        private readonly List<Producer> _producers;

        public ProducerRepo()
        {
            _producers = new List<Producer>();
        }

        public void Add(Producer producer)
        {
            _producers.Add(producer);
        }

        public List<Producer> GetProducer()
        {
            return _producers.ToList();
        }

        public void DeleteProducer(Producer producer)
        {
            _producers.Remove(producer);
        }

    }
}
