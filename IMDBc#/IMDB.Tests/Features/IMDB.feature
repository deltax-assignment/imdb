﻿Feature: IMDB feature

@addMovie
Scenario: Add Movie
	Given I have a move Title "abc"
	And YearOfRelease "2019"
	And Plot "action"
	And Actor "1,2"
	And Producer "1"
	When I add movie
	Then the list should look like
		| Title | YearOfRelease | Plot   | Id |
		| abc   | 2019          | action | 2  |
	And  the actor list should look like
		| Name | DateOfBirth | Id |
		| tom  | 09/09/2009  | 1  |
		| abhi | 08/08/2009  | 2  |
	And the producer list should look like
		| Name  | DateOfBirth | Id |
		| jerry | 09/09/2009  | 1  |

@listMovie
Scenario: List Movie
	Given I have list of movies
	When I fetch my movies
	Then I should have the following movies
		| Title | YearOfRelease | Plot   |
		| abc   | 2019          | action |
		| def   | 2020          | comedy |
	And  the actor list should look like
		| Name | DateOfBirth | Id |
		| tom  | 09/09/2009  | 1  |
		| abhi | 08/08/2009  | 2  |
		| oggy | 01/01/2000  | 3  |
	And the producer list should look like
		| Name  | DateOfBirth | Id |
		| jerry | 09/09/2009  | 1  |
		| abc   | 08/08/2009  | 2  |

@GetActorById
Scenario: Get actor by id
	Given  the actor id "4"
	When i fetch the actor for the actorId
	Then the responce data should look like "Actor not found for given id 4"

@GetProducerById
Scenario: Get produer by id
	Given  the producer Id "2"
	When i fetch producer for the given producerId
	Then the producer list should look like
		| Name | DateOfBirth | Id |
		| abc  | 08/08/2009  | 2  |

@DeleteActorById
Scenario: Delete actor by id
	Given the actorid "2"
	When i delete the actor for the given actor id
	Then the actor list should look like
		| Name | DateOfBirth | Id |
		| tom  | 09/09/2009  | 1  |