﻿using IMDB.Domain;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace IMDB.Tests.Steps
{
    [Binding]
    public sealed class IMDBStepDefinitions
    {

        private string _title, _plot, _producer, _actor, _yearOfRelease;
        private int _producerId, _actorId, _movieId;
        private Exception _exception;
        private List<Movie> _movies;
        private List<Actor> _actors;

        private List<Producer> _producers;
        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef
        private MovieService _movieService;
        private ActorService _actorService;
        private ProducerService _producerService;
        private Producer _prod;
        private Actor _act;
        public IMDBStepDefinitions()
        {
            _actorService = new ActorService();
            _producerService = new ProducerService();
            _movieService = new MovieService(_actorService, _producerService);

        }
        [Given(@"I have a move Title ""(.*)""")]
        public void GivenIHaveAMoveTitle(string title)
        {
            _title = title;
        }

        [Given(@"YearOfRelease ""(.*)""")]
        public void GivenYearOfRelease(string yearOfRelease)
        {
            _yearOfRelease = yearOfRelease;
        }

        [Given(@"Plot ""(.*)""")]
        public void GivenPlot(string plot)
        {
            _plot = plot;
        }

        [Given(@"Actor ""(.*)""")]
        public void GivenActor(string actor)
        {
            _actor = actor;
        }

        [Given(@"Producer ""(.*)""")]
        public void GivenProducer(string producer)
        {
            _producer = producer;
        }

        [When(@"I add movie")]
        public void WhenIAddMovie()
        {
            try
            {
                _movieId = _movieService.AddMovie(_title, _yearOfRelease, _plot, _actor, _producer);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then(@"the list should look like")]
        public void ThenTheListShouldLookLike(Table table)
        {
            if (_movieId > 0)
            {
                _movies = _movieService.GetAll().Where(m => m.Id == _movieId).ToList();

            }
            else
            {
                _movies = _movieService.GetAll();
            }
            table.CompareToSet(_movies);
        }

        [Given(@"I have list of movies")]
        public void GivenIHaveListOfMovies()
        {
        }

        [When(@"I fetch my movies")]
        public void WhenIFetchMyMovies()
        {
            _movies = _movieService.GetAll();
        }

        [Then(@"I should have the following movies")]
        public void ThenIShouldHaveTheFollowingMovies(Table table)
        {

            if (_movieId > 0)
            {
                _movies = _movieService.GetAll().Where(m => m.Id == _movieId).ToList();

            }
            else
            {
                _movies = _movieService.GetAll();
            }
            table.CompareToSet(_movies);
        }

        [Given(@"I have a list of actor")]
        public void GivenIHaveAListOfActor()
        {
        }

        [When(@"i fetch my actors")]
        public void WhenIFetchMyActors()
        {
            _actors = _actorService.GetAll();
        }

        [When(@"select actor id ""(.*)")]
        public void WhenSelectActorId(int actorId)
        {
            try
            {

                _act = _actorService.GetEntity(actorId);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then(@"i should get an error ""(.*)""")]
        public void ThenIShouldGetAnError(string message)
        {
            Assert.AreEqual(message, _exception.Message);

        }

        [Given(@"I have a list of producer")]
        public void GivenIHaveAListOfProducer()
        {
        }

        [When(@"i fetch my producer")]
        public void WhenIFetchMyProducer()
        {
            _producers = _producerService.GetAll();
        }

        [When(@"select producer Id ""(.*)""")]
        public void WhenSelectProducerId(int producerId)
        {
            _prod = _producerService.GetEntity(producerId);
        }




        [Then(@"the actor list should look like")]
        public void ThenTheActorListShouldLookLike(Table table)
        {
            
            if (_movieId > 0)
            {
                _actors = _movies.Where(m => m.Id == _movieId).SelectMany(x => x.Actors).ToList();

            }
            else
            {
                _actors = _actorService.GetAll();
            }
            table.CompareToSet(_actors);

        }

        [Then(@"the producer list should look like")]
        public void ThenTheProducerListShouldLookLike(Table table)
        {
            _producers = new List<Producer>();
            if (_movieId > 0)
            {
                _producers = _movies.Where(m => m.Id == _movieId).Select(p => p.Producer).ToList();
            }
            else if (_prod != null)
            {
                _producers.Add(_prod);
            }
            else
            {
                _producers = _producerService.GetAll();
            }
            table.CompareToSet(_producers);

        }


        [Given(@"the producer Id ""(.*)""")]
        public void GivenTheProducerId(int producerId)
        {
            _producerId = producerId;
        }

        [When(@"i fetch producer for the given producerId")]
        public void WhenIFetchProducerForTheGivenProducerId()
        {
            _prod = _producerService.GetEntity(_producerId);
        }


        [Given(@"the actorid ""(.*)""")]
        public void GivenTheActorid(int actId)
        {
            _actorId = actId;
        }

        [When(@"i delete the actor for the given actor id")]
        public void WhenIDeleteTheActorForTheGivenActorId()
        {
            _actorService.Delete(_actorId.ToString());
        }

        [Given(@"the actor id ""(.*)""")]
        public void GivenTheActorId(int actorId)
        {
            _actorId = actorId;
        }

        [When(@"i fetch the actor for the actorId")]
        public void WhenIFetchTheActorForTheActorId()
        {
            try
            {
                _actorService.GetEntity(_actorId);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then(@"the responce data should look like ""(.*)""")]
        public void ThenTheResponceDataShouldLookLike(string message)
        {
            Assert.AreEqual(message, _exception.Message);
        }


        [BeforeScenario(new string[] { "addMovie", "listMovie" })]
        public void AddSampleProducer()
        {

            _producerService.AddProducer("jerry", "09/09/2009 ");

            _producerService.AddProducer("abc", "08/08/2009");

        }
        [BeforeScenario("GetProducerById")]
        public void AddSampleProduc()
        {

            _producerService.AddProducer("jerry", "09/09/2009 ");
            _producerService.AddProducer("abc", "08/08/2009");

        }
        [BeforeScenario(new string[] { "addMovie", "DeleteActorById", "listMovie" })]
        public void AddActors()
        {

            _actorService.AddActor("tom", "09/09/2009", "1");
            _actorService.AddActor("abhi", "08/08/2009", "2");
        }
        [BeforeScenario("listMovie")]
        public void AddSampleMovie()
        {
            _movieService.AddMovie("abc", "2019", "action", "1,2", "1");
            _actorService.AddActor("oggy", "01/01/2000", "2");

            //  _producerService.AddProducer("adam", "06/05/2005");
            _movieService.AddMovie("def", "2020", "comedy", "3", "1");

        }
        [BeforeScenario("addMovie")]
        public void AddAdditionValues()
        {
            _actorService.AddActor("arun", "01/01/2001", "1");
            _actorService.AddActor("bheem", "02/02/2003", "3");
            _producerService.AddProducer("ayush", "09/09/1990");
            _movieService.AddMovie("last", "2021", "thriller", "3,4", "2");
        }


    }
}
