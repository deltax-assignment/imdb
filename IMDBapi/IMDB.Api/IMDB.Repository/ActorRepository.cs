﻿using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Extensions.Options;
using Dapper;

namespace IMDB.Repository
{
    public class ActorRepository : GenericRepository<Actor>, IActorRepository
    {
        public ActorRepository(IOptions<ConnectionString> connectionString) : base(connectionString.Value)
        {

        }

        public int Add(Actor actor)
        {
            const string sql = @"INSERT INTO  Actors(name,gender,dateofbirth,Bio) OUTPUT INSERTED.[Id] VALUES(@name,@gender,@dateofbirth,@bio)";
            return (int)Create(sql, actor);

        }

        public void Delete(int id)
        {
            const string sql = @"DELETE 
                                  FROM ACTORS 
                                  WHERE id = @id";
            DeleteEntity(sql, id);
        }

        public Actor Get(int id)
        {
            const string sql = @"SELECT * 
                                 FROM ACTORS 
                                 WHERE id = @id";
            return GetEntity(sql, id);
        }

        public int ActorDeletable(int id)
        {
            const string sql = "SELECT count(amm.movieId) FROM ACTORMOVIEMAPPING as amm WHERE ACTORID = @id";
            using var connection = new SqlConnection(_connectionString.DB);
            return connection.Query<int>(sql, new { Id = id }).SingleOrDefault();
        }

        public List<Actor> GetEntities()
        {
            const string sql = @"SELECT * 
                                 FROM actors";
            return (List<Actor>)GetAllData(sql);
        }

        public void Update(Actor actor)
        {
            const string sql = @"UPDATE Actors 
                                 SET NAME = @name,
                                 GENDER = @gender,
                                 DATEOFBIRTH = @dateofbirth,
                                 BIO = @bio 
                                 WHERE ID =@id";
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query<Actor>(sql, new
            {
                actor.Name,
                actor.Gender,
                actor.DateOfBirth,
                actor.Bio,
                actor.Id
            });
        }

        public IEnumerable<Actor> GetActorsByMovie(int movieId)
        {
            const string sql = @"SELECT A.* FROM Actors A JOIN ActorMovieMapping AMM ON A.Id = AMM.ActorId WHERE AMM.MovieId = @id";
            return GetByMovie(sql, movieId);
        }
    }
}
