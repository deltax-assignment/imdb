﻿using Dapper;
using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Repository
{
    public class GenreRepository : GenericRepository<Genre>, IGenreRepository
    {
        public GenreRepository(IOptions<ConnectionString> connectionString) : base(connectionString.Value)
        {
        }

        public int Add(Genre genre)
        {
            const string sql = @"INSERT INTO 
                                 GENRES(name) 
                                 OUTPUT INSERTED.[Id]
                                 VALUES(@name)";
            return (int)Create(sql, genre);
        }
        public int GenreDeletable(int id)
        {
            const string sql = "SELECT count(amm.movieId) FROM GENREMOVIEMAPPING as amm WHERE GENREID = @id";
            using var connection = new SqlConnection(_connectionString.DB);
            return connection.Query<int>(sql, new { Id = id }).SingleOrDefault();
        }
        public void Delete(int id)
        {
            const string sql = @" DELETE 
                                  FROM GENRES 
                                  WHERE id = @id";
            DeleteEntity(sql, id);
        }

        public Genre Get(int id)
        {
            const string sql = @"SELECT * 
                                 FROM GENRES 
                                 WHERE id = @id";
            return GetEntity(sql, id);
        }

        public IEnumerable<Genre> GetEntities()
        {
            const string sql = @"SELECT * 
                                 FROM GENRES";
            return GetAllData(sql);
        }

        public void Update(Genre genre)
        {
            const string sql = @"UPDATE Genres 
                                 SET NAME = @name 
                                 WHERE ID =@id";
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query<Genre>(sql, new
            {
                genre.Name,
                genre.Id
            });
        }
        public IEnumerable<Genre> GetGenresByMovie(int movieId)
        {
            string sql = @"SELECT G.* FROM Genres G JOIN GenreMovieMapping GMM ON G.Id = GMM.GenreId WHERE GMM.MovieId = @id";
            return GetByMovie(sql, movieId);
        }
    }
}
