﻿using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using IMDB.Domain;

namespace IMDB.Repository
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public readonly ConnectionString _connectionString;
        public MovieRepository(IOptions<ConnectionString> connectionString) : base(connectionString.Value)
        {
            _connectionString = connectionString.Value;
        }
        public int Add(Movie movie, string actorIds, string genreIds)
        {
            string storedProcedureName = "usp_AddMovie";
            var parameter = new
            {
                title = movie.Title,
                yearofrelease = movie.YearOfRelease,
                plot = movie.Plot,
                producerid = movie.ProducerId,
                posterlink = movie.PosterLink,
                actorIds = actorIds,
                genreIds = genreIds
            };
            using (var connection = new SqlConnection(_connectionString.DB))
            {
                return connection.Query<int>(storedProcedureName, parameter, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public void Delete(int id)
        {
            const string sql = @"EXEC usp_DeleteMovie @MovieId";
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Execute(sql, new { MovieId = id });
        }

        public Movie Get(int id)
        {
            const string sql = @"SELECT * 
                                 FROM MOVIES 
                                 WHERE id = @id ";
            return GetEntity(sql, id);
        }

        public IEnumerable<Movie> GetBy(int yearOfRelease)
        {
            const string sql = @"SELECT * 
                                 FROM MOVIES 
                                 WHERE YearOfRelease = @yearOfRelease ";
            using (var connection = new SqlConnection(_connectionString.DB))
            {
                return connection.Query<Movie>(sql, new { YearOfRelease = yearOfRelease });
            }

        }
        public IEnumerable<Movie> Paging(int page,int size)
        {
            var pageId = (page - 1) * size;
            const string sql = @"SELECT * FROM movies
                                    ORDER BY id
                                     OFFSET @pageId ROWS
                                    FETCH NEXT @size ROWS ONLY";
          
            using (var connection = new SqlConnection(_connectionString.DB))
            {
                return connection.Query<Movie>(sql, new { pageId = pageId ,size = size});
            }
        }

        public IEnumerable<Movie> GetEntities()
        {
            const string sql = @"SELECT * 
                                 FROM movies";
            return GetAllData(sql);
        }

        public void Update(Movie movie, string actorIds, string genreIds)
        {
            const string sql = @"EXEC [dbo].[usp_UpdateMovie] 
   @MovieId
  ,@title
  ,@yearofrelease
  ,@plot
  ,@producerid
  ,@posterlink
  ,@actorIds
  ,@genreIds";
            using var connection = new SqlConnection(_connectionString.DB);
            var movieReq = new
            {
                MovieId = movie.Id,
                Title = movie.Title,
                Plot = movie.Plot,
                YearOfRelease = movie.YearOfRelease,
                PosterLink = movie.PosterLink,
                ProducerId = movie.ProducerId,
                ActorIds = actorIds,
                GenreIds = genreIds
            };
            connection.Execute(sql,movieReq);
        }
        public void Patch(string sql, DynamicParameters dbArgs)
        {
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Execute(sql, dbArgs);
        }
    }
}
