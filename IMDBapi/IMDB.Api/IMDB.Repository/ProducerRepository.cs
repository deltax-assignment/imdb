﻿using Dapper;
using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Repository
{
    public class ProducerRepository : GenericRepository<Producer>, IProducerRepository
    {
        public ProducerRepository(IOptions<ConnectionString> connectionString) : base(connectionString.Value)
        {
        }

        public int Add(Producer producer)
        {
            const string sql = @"INSERT INTO 
                                 Producers(name,gender,dateofbirth,Bio) 
                                 OUTPUT INSERTED.[Id]
                                 VALUES(@name,@gender,@dateofbirth,@bio)";
            return (int)Create(sql, producer);
        }

        public int ProducerDeletable(int id)
        {
            const string sql = "SELECT count(m.Id) FROM MOVIES as m WHERE PRODUCERID = @id";
            using var connection = new SqlConnection(_connectionString.DB);
            return connection.Query<int>(sql, new { Id = id }).SingleOrDefault();
        }
        public void Delete(int id)
        {
            const string sql = @" DELETE 
                                  FROM PRODUCERS 
                                  WHERE id = @id";
            DeleteEntity(sql, id);
        }

        public Producer Get(int id)
        {
            const string sql = @"SELECT * 
                                 FROM PRODUCERS 
                                 WHERE id = @id";
            return GetEntity(sql, id);
        }

        public IEnumerable<Producer> GetEntities()
        {
            const string sql = @"SELECT * 
                                 FROM PRODUCERS";
            return GetAllData(sql);
        }

        public void Update(Producer producer)
        {
            const string sql = @"UPDATE Producers 
                                 SET NAME = @name,
                                 GENDER = @gender,
                                 DATEOFBIRTH = @dateofbirth,
                                 BIO = @bio 
                                 WHERE ID =@id";
            using var connection = new SqlConnection(_connectionString.DB);
            connection.Query<Producer>(sql, new
            {
                producer.Name,
                producer.Gender,
                producer.DateOfBirth,
                producer.Bio,
                producer.Id
            });
        }
    }
}
