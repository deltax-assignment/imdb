﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IMDB.Repository
{
    public class GenericRepository<T>
    {
        public readonly ConnectionString _connectionString;
        public GenericRepository(ConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<T> GetAllData(string sql)
        {
            using (var connection = new SqlConnection(_connectionString.DB))
            {
                return connection.Query<T>(sql);
            }
        }

        public T GetEntity(string sql, int id)
        {
            using (var connection = new SqlConnection(_connectionString.DB))
            {
                return connection.Query<T>(sql, new { Id = id }).SingleOrDefault();
            }
        }

        public long Create(string sql, T entity)
        {
            using (var connection = new SqlConnection(_connectionString.DB))
            {
                return connection.Query<long>(sql, entity).SingleOrDefault();
            }
        }

        public void DeleteEntity(string sql, int entity)
        {
            using (var connection = new SqlConnection(_connectionString.DB))
            {
                connection.Query<T>(sql, new { Id = entity });
            }
        }

        public IEnumerable<T> GetByMovie(string sql, int movieId)
        {
            using (var connection = new SqlConnection(_connectionString.DB))
            {
                return connection.Query<T>(sql, new { Id = movieId });
            }
        }
    }
}
