﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IMDB.Domain.Entity;

namespace IMDB.Repository.Interface
{
    public interface IActorRepository
    {
        public List<Actor> GetEntities();

        public Actor Get(int id);

        public int Add(Actor actor);

        public void Delete(int id);

        public void Update(Actor actor);

        public int ActorDeletable(int id);
        public IEnumerable<Actor> GetActorsByMovie(int movieId);

    }
}
