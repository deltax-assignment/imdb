﻿using IMDB.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Repository.Interface
{
    public interface IProducerRepository
    {
        public IEnumerable<Producer> GetEntities();

        public Producer Get(int id);

        public int Add(Producer producer);

        public int ProducerDeletable(int id);

        public void Delete(int id);

        public void Update(Producer produer);


    }
}
