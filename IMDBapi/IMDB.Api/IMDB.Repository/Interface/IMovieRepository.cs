﻿using Dapper;
using IMDB.Domain;
using IMDB.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Repository.Interface
{
    public interface IMovieRepository
    {
        public IEnumerable<Movie> GetEntities();

        public Movie Get(int Id);
        public IEnumerable<Movie> Paging(int page, int size);

        public IEnumerable<Movie> GetBy(int yearOfRelease);

        public int Add(Movie movie, string actorIds, string genreIds);

        public void Delete(int id);

        public void Update(Movie movie, string actorIds, string genreIds);

        public void Patch(string sql, DynamicParameters q);

    }
}
