﻿using IMDB.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Repository.Interface
{
    public interface IGenreRepository
    {
        public IEnumerable<Genre> GetEntities();

        public Genre Get(int id);

        public int Add(Genre genre);

        public int GenreDeletable(int id);

        public void Delete(int id);

        public void Update(Genre genre);

        public IEnumerable<Genre> GetGenresByMovie(int movieId);
    }
}
