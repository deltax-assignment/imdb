﻿namespace IMDB.Domain.Entity
{
    public enum Gender
    {
        male = 1,
        female = 2
    }
}