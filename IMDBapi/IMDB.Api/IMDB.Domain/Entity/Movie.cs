﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Domain.Entity
{
    public class Movie
    {

        public int Id { get; set; }

        public string Title { get; set; }

        public int YearOfRelease { get; set; }

        public string Plot { get; set; }

        public int ProducerId { get; set; }

        public string PosterLink { get; set; }


    }
}
