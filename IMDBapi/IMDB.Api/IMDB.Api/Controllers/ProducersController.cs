﻿using IMDB.Api.Models.Request;
using IMDB.Api.Services.Interface;
using IMDB.Domain.Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class ProducersController : ControllerBase
    {
        private readonly IProducerService _producerService;

        public ProducersController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpGet]
        public IActionResult GetEntities()
        {
            try
            {
                var producers = _producerService.GetEntities();
                return Ok(new { data = producers });
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }

        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var producer = _producerService.GetEntity(id);
                return Ok(new { data = producer });
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] AddProducerRequest producer)
        {
            try
            {
                var id = _producerService.Add(producer.Name, producer.Gender, producer.DateOfBirth, producer.Bio);
                return Ok(new { data = id });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] AddProducerRequest producer)
        {
            try
            {
                _producerService.Update(producer.Name, producer.Gender, producer.DateOfBirth, producer.Bio, id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _producerService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
