﻿using IMDB.Api.Models.Request;
using IMDB.Api.Services.Interface;
using IMDB.Domain;
using IMDB.Domain.Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        public IActionResult GetEntities(string yearOfRelease,string pageNumber,string range)
        {
            try
            {
                var movies = _movieService.GetEntities(yearOfRelease,pageNumber,range);
                return Ok(new { data = movies });
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var movie = _movieService.GetEntity(id);
                return Ok(new { data = movie });
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPost]
        public IActionResult Post([FromForm] AddMovieRequest movie)
        {
            try
            {
                var id = _movieService.Add(movie.Name, movie.YearOfRelease, movie.Plot, movie.ProducerId, movie.ActorIds, movie.PosterLink, movie.GenreIds);
                return Ok(new { data = id });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromForm] AddMovieRequest movie)
        {
            try
            {
                _movieService.Update(movie.Name, movie.YearOfRelease, movie.Plot, movie.ProducerId, movie.ActorIds, movie.PosterLink, movie.GenreIds, id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpPatch("{id}")]
        public IActionResult Patch(int id, [FromBody] string patchReq)
        {
            try
            {
                _movieService.Patch(id, patchReq);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _movieService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
