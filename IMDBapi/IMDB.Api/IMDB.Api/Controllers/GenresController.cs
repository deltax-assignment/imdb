﻿using IMDB.Api.Models.Request;
using IMDB.Api.Services.Interface;
using IMDB.Domain.Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class GenresController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenresController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        public IActionResult GetEntities()
        {
            try
            {
                var genres = _genreService.GetEntities();
                return Ok(new { data = genres });
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var genre = _genreService.GetEntity(id);
                return Ok(new { data = genre });
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] AddGenreRequest genre)
        {
            try
            {
                int id = _genreService.Add(genre.Name);
                return Ok(new { data = id });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] AddGenreRequest genre)
        {
            try
            {
                _genreService.Update(genre.Name, id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _genreService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
