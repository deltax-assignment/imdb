﻿using IMDB.Api.Models.Request;
using IMDB.Api.Services.Interface;
using IMDB.Domain.Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class ActorsController : ControllerBase
    {
        private readonly IActorService _actorService;

        public ActorsController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        public IActionResult GetEntities()
        {
            try
            {
                var actors = _actorService.GetEntities();
                return Ok(new { data = actors });
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var actor = _actorService.GetEntity(id);
                return Ok(new { data = actor });
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] AddActorRequest actor)
        {

                var id = _actorService.Add(actor.Name, actor.Gender, actor.DateOfBirth, actor.Bio);
                return Ok(new { data = id });
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] AddActorRequest actor)
        {
            try
            {
                _actorService.Update(actor.Name, actor.Gender, actor.DateOfBirth, actor.Bio, id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _actorService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}
