﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Models.DB
{
    public class GenreDB
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
