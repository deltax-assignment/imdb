﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Models.DB
{
    public class MovieDB
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime YearOfRelease { get; set; }

        public int ProducerId { get; set; }
        public string Plot { get; set; }

        public string PosterLink { get; set; }

    }
}
