﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Models.Request
{
    public class AddProducerRequest
    {

        public string Name { get; set; }

        public int Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Bio { get; set; }

    }
}
