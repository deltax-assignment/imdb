﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Models.Request
{
    public class AddMovieRequest
    {
        public string Name { get; set; }

        public int YearOfRelease { get; set; }

        public int ProducerId { get; set; }

        public string Plot { get; set; }

        public IFormFile PosterLink { get; set; }

        public List<int> ActorIds { get; set; }

        public List<int> GenreIds { get; set; }

    }
}
