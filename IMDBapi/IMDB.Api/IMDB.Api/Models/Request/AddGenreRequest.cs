﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Models.Request
{
    public class AddGenreRequest
    {

        public string Name { get; set; }

    }
}
