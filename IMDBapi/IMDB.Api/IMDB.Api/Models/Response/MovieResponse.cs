﻿using IMDB.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Models.Response
{
    public class MovieResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Plot { get; set; }

        public int YearOfRelease { get; set; }

        public Producer Producer { get; set; }

        public string PosterLink { get; set; }

        public List<Actor> Actors { get; set; }

        public List<Genre> Genres { get; set; }
    }
}
