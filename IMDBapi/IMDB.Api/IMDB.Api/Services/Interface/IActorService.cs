﻿using IMDB.Api.Models.DB;
using IMDB.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Services.Interface
{
    public interface IActorService
    {
        public int Add(string name, Gender gender, DateTime birth, string bio);

        public List<Actor> GetEntities();

        public Actor GetEntity(int id);

        public void Delete(int id);

        public void ActorDeletable(int id);

        public void Update(string name, Gender gender, DateTime birth, string bio, int id);

        public IEnumerable<Actor> GetActorsByMovie(int movieId);

    }
}
