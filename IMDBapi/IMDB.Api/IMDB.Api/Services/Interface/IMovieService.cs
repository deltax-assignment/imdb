﻿using IMDB.Api.Models.DB;
using IMDB.Api.Models.Request;
using IMDB.Api.Models.Response;
using IMDB.Domain;
using IMDB.Domain.Entity;
using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Services.Interface
{
    public interface IMovieService
    {

        public int Add(string name, int yearOfRelease, string plot, int producerId, List<int> actorIds, IFormFile posterLink, List<int> genreIds);

        public MovieResponse GetEntity(int id);

        public IEnumerable<MovieResponse> GetEntities(string year,string pageNumber,string range);

        public void Delete(int id);

        public void Update(string name, int yearOfRelease, string plot, int producerId, List<int> actorIds, IFormFile posterLink, List<int> genreIds, int id);

        public void Patch(int id, string movie);

    }
}
