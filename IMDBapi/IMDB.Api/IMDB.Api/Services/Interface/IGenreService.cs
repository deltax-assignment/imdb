﻿using IMDB.Api.Models.DB;
using IMDB.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Services.Interface
{
    public interface IGenreService
    {
        public int Add(string name);

        public List<Genre> GetEntities();

        public Genre GetEntity(int id);

        public void GenreDeletable(int id);

        public void Delete(int id);

        public void Update(string name, int id);

        public IEnumerable<Genre> GetGenresByMovie(int movieId);

    }
}
