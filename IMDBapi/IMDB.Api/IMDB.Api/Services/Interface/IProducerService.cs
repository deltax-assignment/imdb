﻿using IMDB.Api.Models.DB;
using IMDB.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Services.Interface
{
    public interface IProducerService
    {

        public int Add(string name, int gender, DateTime birth, string bio);

        public List<Producer> GetEntities();

        public Producer GetEntity(int id);

        public void Delete(int id);

        public void Update(string name, int gender, DateTime birth, string bio, int id);

    }
}
