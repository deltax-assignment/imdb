﻿using Dapper;
using IMDB.Api.Models.Request;
using IMDB.Api.Models.Response;
using IMDB.Api.Services.Interface;
using IMDB.Domain;
using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace IMDB.Api.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IProducerRepository _producerRepository;
        private readonly IActorService _actorService;
        private readonly IGenreService _genreService;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public MovieService(IMovieRepository movieRepository, IProducerRepository producerRepository,
            IActorService actorService, IGenreService genreService, IWebHostEnvironment webHostEnvironment)
        {
            _movieRepository = movieRepository;
            _producerRepository = producerRepository;
            _actorService = actorService;
            _genreService = genreService;
            _webHostEnvironment = webHostEnvironment;
        }

        public void Validate(string name, int yearOfRelease, string plot, int producerId, List<int> actorIds)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Movie name is empty");
            }
            if (string.IsNullOrEmpty(plot))
            {
                throw new Exception("Movie plot is empty");
            }
            if (producerId < 1)
            {
                throw new Exception("Producer id is invalid");
            }
            if (yearOfRelease < 1900)
            {
                throw new Exception("Year oof release not valid");
            }
        }

        public void ValidateActorList(List<int> actorsIds)
        {
            foreach (int i in actorsIds)
            {
                _actorService.GetEntity(i);
            }
        }

        public void ValidateGenreList(List<int> genreIds)
        {
            foreach (int i in genreIds)
            {
                _genreService.GetEntity(i);
            }
        }

        private string UploadedFile(IFormFile posterLink)
        {
            string uniqueFileName = null;

            if (posterLink != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + posterLink.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                   posterLink.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
        public int Add(string name, int yearOfRelease, string plot, int producerId, List<int> actorIds, IFormFile posterLink, List<int> genreIds)
        {
            string posterLinkFilePath = UploadedFile(posterLink);
            Validate(name, yearOfRelease, plot, producerId, actorIds);
            var movie = new Movie()
            {
                Title = name,
                YearOfRelease = yearOfRelease,
                Plot = plot,
                PosterLink = posterLinkFilePath,
                ProducerId = producerId

            };
            ValidateActorList(actorIds);
            ValidateGenreList(genreIds);
            return _movieRepository.Add(movie, string.Join(',', actorIds), string.Join(',', genreIds));
        }

        public IEnumerable<MovieResponse> GetEntities(string year, string pageNumber, string range)
        {
            IEnumerable<Movie> movies;
            if (year != null)
            {
                int yearOfRelease = 0;
                if (Int32.TryParse(year, out int value) && value > 1900)
                {
                    yearOfRelease = value;
                }
                movies = _movieRepository.GetBy(yearOfRelease);
            }else if(pageNumber != null && range != null)
            {
                int page = 0,size = 0;
                if (Int32.TryParse(pageNumber, out int value) && value > 0)
                {
                    page = value;
                }
                if (Int32.TryParse(range, out int r) && value > 0)
                {
                    size = r;
                }
                movies = _movieRepository.Paging(page, size);

            }
            else
            {
                movies = _movieRepository.GetEntities();
            }
            return movies.Select(m => new MovieResponse
            {
                Id = m.Id,
                Name = m.Title,
                YearOfRelease = m.YearOfRelease,
                Plot = m.Plot,
                PosterLink = m.PosterLink,
                Producer = _producerRepository.Get(m.ProducerId),
                Actors = _actorService.GetActorsByMovie(m.Id).ToList(),
                Genres = _genreService.GetGenresByMovie(m.Id).ToList()
            });
        }

        public MovieResponse GetEntity(int id)
        {
            if (id <= 0)
            {
                throw new Exception("Movieid Invalid");
            }
            var movie = _movieRepository.Get(id);
            if (movie == null)
            {
                throw new Exception($"Movie not found for given id {id}");
            }
            return new MovieResponse
            {
                Id = id,
                Name = movie.Title,
                YearOfRelease = movie.YearOfRelease,
                Plot = movie.Plot,
                PosterLink = movie.PosterLink,
                Producer = _producerRepository.Get(movie.ProducerId),
                Actors = _actorService.GetActorsByMovie(movie.Id).ToList(),
                Genres = _genreService.GetGenresByMovie(movie.Id).ToList()
            };
        }

        public void Delete(int id)
        {
            GetEntity(id);
            _movieRepository.Delete(id);
        }

        public void Update(string name, int yearOfRelease, string plot, int producerId, List<int> actorIds, IFormFile posterLink, List<int> genreIds, int id)
        {
            string posterLinkFilePath = UploadedFile(posterLink);
            Validate(name, yearOfRelease, plot, producerId, actorIds);
            var movie = new Movie()
            {
                Title = name,
                YearOfRelease = yearOfRelease,
                Plot = plot,
                PosterLink = posterLinkFilePath,
                ProducerId = producerId,
                Id = id

            };
            GetEntity(id);
            ValidateActorList(actorIds);
            ValidateGenreList(genreIds);
            _movieRepository.Update(movie, String.Join(',', actorIds), String.Join(',', genreIds));

        }

        public void Patch(int id, string patchReq)
        {
            GetEntity(id);
            //   var patch = JObject.Parse(patchReq);
            var patch = JArray.Parse(patchReq);
            var dbArg = new DynamicParameters();
            List<string> fields = new List<string>();
            foreach (var p in patch)
            {
                var key = p["name"].ToString().ToUpper();
                var value = p["value"].ToString();
                switch (key)
                {
                    case "PLOT":
                        dbArg.Add("@Plot ", value);
                        fields.Add("Plot = @Plot");
                        break;
                    case "YEAROFRELEASE":
                        dbArg.Add("@YearOfRelease ", value);
                        fields.Add("YearOfRelease = @YearOfRelease");
                        break;
                    case "NAME":
                        dbArg.Add("@Name ", value);
                        fields.Add("Title = @Name");
                        break;
                    case "PRODUCERID":
                        dbArg.Add("@ProducerId ", value);
                        fields.Add("ProducerId = @ProducerId");
                        break;
                    case "ActorIds":
                        break;
                    case "GenreIds":
                        break;
                    default:
                        throw new Exception("invalid input try to dbug");
                }
       
            }
          
            dbArg.Add("@movieId", id);
          
               string sql = $"update movies set {string.Join(",", fields) } where id = @movieId";
              _movieRepository.Patch(sql, dbArg);
        }
    }
}
