﻿using IMDB.Api.Services.Interface;
using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;

        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }

        public int Add(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Genre name is empty");
            }
            var genre = new Genre()
            {
                Name = name
            };
            return _genreRepository.Add(genre);
        }

        public List<Genre> GetEntities()
        {
            return _genreRepository.GetEntities().ToList();
        }

        public Genre GetEntity(int id)
        {
            if (id <= 0)
            {
                throw new Exception("Genreid Invalid");
            }
            var genre = _genreRepository.Get(id);
            if (genre == null)
            {
                throw new Exception($"Genre not found for given id {id}");
            }
            return (Genre)genre;
        }

        public void GenreDeletable(int id)
        {
            var count = _genreRepository.GenreDeletable(id);
            if (count > 0)
            {
                throw new Exception($"Actor with id {id} is already present in a movie");
            }
        }

        public void Delete(int id)
        {
            GetEntity(id);
            GenreDeletable(id);
            _genreRepository.Delete(id);
        }

        public void Update(string name, int id)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Genre name is empty");
            }
            var genre = new Genre()
            {
                Id = id,
                Name = name,
            };
            GetEntity(id);
            _genreRepository.Update(genre);
        }

        public IEnumerable<Genre> GetGenresByMovie(int movieId)
        {
            return _genreRepository.GetGenresByMovie(movieId);
        }
    }
}
