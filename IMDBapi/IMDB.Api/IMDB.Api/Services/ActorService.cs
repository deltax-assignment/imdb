﻿using IMDB.Api.Models.DB;
using IMDB.Api.Services.Interface;
using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;

        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }

        public void validate(string name, string bio)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Actor name is empty");
            }
            if (string.IsNullOrEmpty(bio))
            {
                throw new Exception("Actor bio is empty");
            }
        }

        public int Add(string name, Gender gender, DateTime birth, string bio)
        {

            validate(name, bio);
            var actor = new Actor()
            {
                Name = name,
                Gender = (Gender)gender,
                DateOfBirth = birth,
                Bio = bio
            };
            return _actorRepository.Add(actor);
        }

        public List<Actor> GetEntities()
        {
            return _actorRepository.GetEntities().ToList();
        }

        public Actor GetEntity(int id)
        {
            if (id <= 0)
            {
                throw new Exception("Actorid Invalid");
            }
            var actor = _actorRepository.Get(id);
            if (actor == null)
            {
                throw new Exception($"Actor not found for given id {id}");
            }
            return (Actor)actor;
        }

        public void Delete(int id)
        {
            GetEntity(id);
            ActorDeletable(id);
            _actorRepository.Delete(id);
        }

        public void ActorDeletable(int id)
        {
            var count = _actorRepository.ActorDeletable(id);
            if (count > 0)
            {
                throw new Exception($"Actor with id {id} is already present in a movie");
            }
        }
        public void Update(string name, Gender gender, DateTime birth, string bio, int id)
        {
            validate(name, bio);
            var actor = new Actor()
            {
                Id = id,
                Name = name,
                DateOfBirth = birth,
                Bio = bio,
                Gender = (Gender)gender
            };
            GetEntity(id);
            _actorRepository.Update(actor);
        }

        public IEnumerable<Actor> GetActorsByMovie(int movieId)
        {
            return _actorRepository.GetActorsByMovie(movieId);
        }

    }
}
