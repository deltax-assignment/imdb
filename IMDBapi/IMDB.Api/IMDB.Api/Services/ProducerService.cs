﻿using IMDB.Api.Services.Interface;
using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDB.Api.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;

        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }

        public void validate(string name, string bio)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("Producer name is empty");
            }
            if (string.IsNullOrEmpty(bio))
            {
                throw new Exception("Producer bio is empty");
            }
        }

        public int Add(string name, int gender, DateTime birth, string bio)
        {

            validate(name, bio);
            var producer = new Producer()
            {
                Name = name,
                Gender = (Gender)gender,
                DateOfBirth = birth,
                Bio = bio
            };
            return _producerRepository.Add(producer);
        }

        public List<Producer> GetEntities()
        {
            return _producerRepository.GetEntities().ToList();
        }

        public Producer GetEntity(int id)
        {
            if (id <= 0)
            {
                throw new Exception("Producerid Invalid");
            }
            var producer = _producerRepository.Get(id);
            if (producer == null)
            {
                throw new Exception($"Producer not found for given id {id}");
            }
            return producer;
        }
        public void ProducerDeletable(int id)
        {
            var count = _producerRepository.ProducerDeletable(id);
            if (count > 0)
            {
                throw new Exception($"Actor with id {id} is already present in a movie");
            }
        }

        public void Delete(int id)
        {
            GetEntity(id);
            ProducerDeletable(id);
            _producerRepository.Delete(id);
        }

        public void Update(string name, int gender, DateTime birth, string bio, int id)
        {
            validate(name, bio);
            var producer = new Producer()
            {
                Id = id,
                Name = name,
                DateOfBirth = birth,
                Bio = bio,
                Gender = (Gender)gender
            };
            _producerRepository.Update(producer);
        }
    }
}
