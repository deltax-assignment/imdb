﻿Feature: Genre

@GetAllGenres
Scenario: Get All Genres
    Given I am a client
    When I make GET Request '/genres'
    Then response code must be '200'
    And response data must look like '{"data":[{"id":1,"name":"Action"},{"id":2,"name":"fun"}]}'

@GetGenreById
Scenario: Get Genre by id
    Given I am a client
    When I make a GET Request '/genres/1'
    Then response code must be '200'
    And response data must look like '{"data":{"id":1,"name":"Action"}}'

@PostGenre
Scenario: Post Genre
Given I am a client
When I make a POST Request '/genres' and the data is '{"name":"comedy"}'
Then response code must be '200'
And respose data must look like '{"data":1}'

@UpdateGenre
Scenario: Update Genre
Given I am a client
When I make a PUT Request '/genres/1' and the data is '{"name":"funny"}'
Then response code must be '200'

@DeleteGenre
Scenario: Delete Genre
Given I am a client
When I make a DELETE Request '/genres/1'
Then response code must be '200'
