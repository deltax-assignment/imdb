﻿Feature: Movie

@GetAllMovies
Scenario: Get All Movies
    Given I am a client
    When I make GET Request '/movies'
    Then response code must be '200'
    And response data must look like '{"data":[{"id":1,"name":"up","plot":"American","yearOfRelease":2011,"producer":{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1},"posterLink":"poster-url","actors":[{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1},{"id":2,"name":"Matt Damon","bio":"American","dateOfBirth":"1970-10-08T00:00:00","gender":1}],"genres":[{"id":1,"name":"Action"},{"id":2,"name":"fun"}]}]}'

@GetMovieByYearOfRelease
Scenario: Get Movie By Yearofrelease
    Given I am a client
    When  I make GET Request '/movies?yearOfRelease=2011'
    Then response code must be '200'
    And response data must look like '{"data":[{"id":1,"name":"up","plot":"American","yearOfRelease":2011,"producer":{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1},"posterLink":"poster-url","actors":[{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1},{"id":2,"name":"Matt Damon","bio":"American","dateOfBirth":"1970-10-08T00:00:00","gender":1}],"genres":[{"id":1,"name":"Action"},{"id":2,"name":"fun"}]}]}'


@GetMovieById
Scenario: Get Movie by id
    Given I am a client
    When I make a GET Request '/movies/1'
    Then response code must be '200'
    And response data must look like '{"data":{"id":1,"name":"up","plot":"American","yearOfRelease":2011,"producer":{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1},"posterLink":"poster-url","actors":[{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1},{"id":2,"name":"Matt Damon","bio":"American","dateOfBirth":"1970-10-08T00:00:00","gender":1}],"genres":[{"id":1,"name":"Action"},{"id":2,"name":"fun"}]}}'

@PostMovie
Scenario: Post Movie
Given I am a client
When I make a POST Request '/movies' and the data is '{"name":"Ford v Ferrari","yearOfRelease":2019,"producerId":1,"plot":"laws","posterlink":"poster-url","actorIds":[2],"genreIds":[1,2]}'
Then response code must be '200'
And respose data must look like '{"data":1}'

@UpdateMovie
Scenario: Update Movie
Given I am a client
When I make a PUT Request '/movies/1' and the data is '{"name":"up","plot":"American","yearOfRelease":2011,"producerId" :1,"posterlink":"poster-url","actorIds":[2],"genreIds":[1]}'
Then response code must be '200'

@DeleteMovie
Scenario: Delete Movie
Given I am a client
When I make a DELETE Request '/movies/1'
Then response code must be '200'