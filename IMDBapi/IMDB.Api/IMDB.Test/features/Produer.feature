﻿
Feature: Producer

@GetAllProducers
Scenario: Get All Producers
    Given I am a client
    When I make GET Request '/producers'
    Then response code must be '200'
    And response data must look like '{"data":[{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1},{"id":2,"name":"Mila Kunis","bio":"Ukranian","dateOfBirth":"1973-06-22T00:00:00","gender":2},{"id":3,"name":"James Mangold","bio":"American","dateOfBirth":"1963-12-16T00:00:00","gender":1}]}'

@GetProducerById
Scenario: Get Producer by id
    Given I am a client
    When I make a GET Request '/producers/1'
    Then response code must be '200'
    And response data must look like '{"data":{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1}}'

@PostProducer
Scenario: Post Producer
Given I am a client
When I make a POST Request '/producers' and the data is '{"name":"Abhi","gender":1,"dateOfBirth":"1999-03-02T00:00:00","bio":"fun"}'
Then response code must be '200'
And respose data must look like '{"data":1}'

@UpdateProducer
Scenario: Update Producer
Given I am a client
When I make a PUT Request '/producers/1' and the data is '{"name":"Abhi","gender":1,"dateOfBirth":"1999-03-02T00:00:00","bio":"fun"}'
Then response code must be '200'

@DeleteProducer
Scenario: Delete Producer
Given I am a client
When I make a DELETE Request '/producers/1'
Then response code must be '200'
