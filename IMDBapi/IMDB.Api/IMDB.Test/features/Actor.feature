﻿Feature: Actor

@GetAllActors
Scenario: Get All Actors
    Given I am a client
    When I make GET Request '/actors'
    Then response code must be '200'
    And response data must look like '{"data":[{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1},{"id":2,"name":"Mila Kunis","bio":"Ukranian","dateOfBirth":"1973-06-22T00:00:00","gender":2}]}'

@GetActorById
Scenario: Get Actor by id
    Given I am a client
    When I make a GET Request '/actors/1'
    Then response code must be '200'
    And response data must look like '{"data":{"id":1,"name":"Christian Bale","bio":"British","dateOfBirth":"1979-03-02T00:00:00","gender":1}}'

@PostActor
Scenario: Post Actor
Given I am a client
When I make a POST Request '/actors' and the data is '{"name":"Abhi","gender":1,"dateOfBirth":"1999-03-02T00:00:00","bio":"fun"}'
Then response code must be '200'
And respose data must look like '{"data":1}'

@UpdateActor
Scenario: Update Actor
Given I am a client
When I make a PUT Request '/actors/1' and the data is '{"name":"Abhi","gender":1,"dateOfBirth":"1999-03-02T00:00:00","bio":"fun"}'
Then response code must be '200'

@DeleteActor
Scenario: Delete Actor
Given I am a client
When I make a DELETE Request '/actors/1'
Then response code must be '200'
