﻿using IMDB.Api;
using IMDB.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;

namespace IMDB.Test
{
    [Scope(Feature = "Movie")]
    [Binding]
    public class MovieSteps : BaseSteps
    {
        public MovieSteps(CustomWebApplicationFactory<TestStartup> factory) : base(factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureServices(services =>
            {
                services.AddScoped(service => MovieMocks.movieRepoMock.Object);
                services.AddScoped(services => MovieMocks.actorRepoMock.Object);
                services.AddScoped(services => MovieMocks.producerRepoMock.Object);
                services.AddScoped(services => MovieMocks.genreRepoMock.Object);

            });
        }))
        {

        }
        [BeforeScenario]
        public void MockRepositories()
        {
            MovieMocks.MockMovieRepo();
           
        }

    }
}