﻿using IMDB.Api;
using IMDB.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;

namespace IMDB.Test
{
    [Scope(Feature = "Producer")]
    [Binding]
    public class ProducerSteps : BaseSteps
    {
        public ProducerSteps(CustomWebApplicationFactory<TestStartup> factory) : base(factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureServices(services =>
            {
                services.AddScoped(service => ProducerMocks.producerRepoMock.Object);
            });
        }))
        {

        }
        [BeforeScenario]
        public void MockRepositories()
        {
            ProducerMocks.MockProducerRepo();
        }

    }
}