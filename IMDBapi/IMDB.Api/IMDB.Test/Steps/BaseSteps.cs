﻿using IMDB.Api;
using IMDB.Domain.Entity;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Xunit;

namespace IMDB.Test
{
    public class BaseSteps
    {
        protected HttpClient Client;
        protected WebApplicationFactory<TestStartup> baseFactory;
        protected HttpResponseMessage _httpResponseMessage;


        public BaseSteps(WebApplicationFactory<TestStartup> baseFactory)
        {
            this.baseFactory = baseFactory;
        }

        [Given(@"I am a client")]
        public void GivenIAmAClient()
        {
            Client = baseFactory.CreateClient(new WebApplicationFactoryClientOptions
            {
                BaseAddress = new Uri("http://localhost")
            });
        }

        [When(@"I make GET Request '(.*)'")]
        public async Task WhenIMakeGETRequest(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            _httpResponseMessage = await Client.GetAsync(uri);
        }

        [Then(@"response code must be '(.*)'")]
        public void ThenResponseCodeMustBe(int statusCode)
        {
            Assert.Equal((HttpStatusCode)statusCode, _httpResponseMessage.StatusCode);
        }

        [Then(@"response data must look like '(.*)'")]
        public async Task ThenResponseDataMustLookLikeAsync(string response)
        {
            var responseData = await _httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            Assert.Equal(responseData, response);
        }

        [When(@"I make a GET Request '(.*)'")]
        public async Task WhenIMakeAGETRequest(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            _httpResponseMessage = await Client.GetAsync(uri);
        }

        [Then(@"respose data must look like '(.*)'")]
        public async Task ThenResposeDataMustLookLikeAsync(string response)
        {
            var responseData = await _httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            Assert.Equal(responseData, response);
        }


        [When(@"I make a POST Request '(.*)' and the data is '(.*)'")]
        public async Task WhenIMakeAPOSTRequestAndTheDataIsAsync(string endPoint, string data)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            using var content = new StringContent(data, Encoding.UTF8, "application/json");
            _httpResponseMessage = await Client.PostAsync(uri, content);
        }

        [When(@"I make a PUT Request '(.*)' and the data is '(.*)'")]
        public async Task WhenIMakeAPUTRequestAndTheDataIsAsync(string endPoint, string data)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            var content = new StringContent(data, Encoding.UTF8, "application/json");
            _httpResponseMessage = await Client.PutAsync(uri, content);
        }

        [When(@"I make a DELETE Request '(.*)'")]
        public async Task WhenIMakeADELETERequestAsync(string endPoint)
        {
            var uri = new Uri(endPoint, UriKind.Relative);
            _httpResponseMessage = await Client.DeleteAsync(uri);
        }

        //[Then(@"response data must look like  '(.*)'")]
        //public async Task ThenResponDataMustLookLike(string response)
        //{
        //    var responseData = await _httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
        //    Assert.Equal(responseData, response);
        //}


    }
}
