﻿using IMDB.Api;
using IMDB.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;

namespace IMDB.Test
{
    [Scope(Feature = "Genre")]
    [Binding]
    public class GenreSteps : BaseSteps
    {
        public GenreSteps(CustomWebApplicationFactory<TestStartup> factory) : base(factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureServices(services =>
            {
                services.AddScoped(service => GenreMocks.genreRepoMock.Object);
            });
        }))
        {

        }
        [BeforeScenario]
        public void MockRepositories()
        {
            GenreMocks.MockGenreRepo();
        }

    }
}