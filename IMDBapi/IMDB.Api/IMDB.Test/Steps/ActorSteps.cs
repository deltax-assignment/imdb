﻿using IMDB.Api;
using IMDB.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;

namespace IMDB.Test
{
    [Scope(Feature = "Actor")]
    [Binding]
    public class ActorSteps : BaseSteps
    {
        public ActorSteps(CustomWebApplicationFactory<TestStartup> factory) : base(factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureServices(services =>
            {
                services.AddScoped(service => ActorMocks.actorRepoMock.Object);
            });
        }))
        {

        }
        [BeforeScenario]
        public void MockRepositories()
        {
            ActorMocks.MockActorRepo();
        }

    }
}