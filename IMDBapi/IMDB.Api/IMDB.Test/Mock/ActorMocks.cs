﻿using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Test
{
    class ActorMocks
    {
        public static readonly Mock<IActorRepository> actorRepoMock = new Mock<IActorRepository>();


        public static readonly List<Actor> _actors = new()
        {
            new Actor()
            {
                Id = 1,
                Name = "Christian Bale",
                Bio = "British",
                DateOfBirth = new DateTime(1979, 03, 02),
                Gender = (Gender)1
            },
            new Actor()
            {
                Id = 2,
                Name = "Mila Kunis",
                Bio = "Ukranian",
                DateOfBirth = new DateTime(1973, 06, 22),
                Gender = (Gender)2
            }
        };

        public static void MockActorRepo()
        {
            actorRepoMock.Setup(repo => repo.GetEntities()).Returns(() =>
            {
                return _actors;
            });
            actorRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return _actors.SingleOrDefault(m => m.Id == id);
            });
            actorRepoMock.Setup(repo => repo.ActorDeletable(It.IsAny<int>())).Returns((int id) => {
                return 0;
            });
            actorRepoMock.Setup(repo => repo.Add(It.IsAny<Actor>())).Returns(1);
            actorRepoMock.Setup(repo => repo.Update(It.IsAny<Actor>()));
            actorRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
    }
}
