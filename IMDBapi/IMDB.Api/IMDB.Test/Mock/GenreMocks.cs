﻿using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Test
{
    class GenreMocks
    {
        public static readonly Mock<IGenreRepository> genreRepoMock = new Mock<IGenreRepository>();

        public static readonly List<Genre> _genres = new()
        {
            new Genre()
            {
                Id = 1,
                Name = "Action",

            },
            new Genre()
            {
                Id = 2,
                Name = "fun",

            }
        };

        public static void MockGenreRepo()
        {
            genreRepoMock.Setup(repo => repo.GetEntities()).Returns(() =>
            {
                return _genres;
            });
            genreRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return _genres.SingleOrDefault(m => m.Id == id);
            });
            genreRepoMock.Setup(repo => repo.Add(It.IsAny<Genre>())).Returns(1);
            genreRepoMock.Setup(repo => repo.Update(It.IsAny<Genre>()));
            genreRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
    }
}
