﻿using IMDB.Api.Models.Response;
using IMDB.Api.Services.Interface;
using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Test
{
    public static class MovieMocks
    {
 
        public static readonly Mock<IMovieRepository> movieRepoMock = new Mock<IMovieRepository>();
        public static readonly Mock<IActorRepository> actorRepoMock = new Mock<IActorRepository>();
        public static readonly Mock<IProducerRepository> producerRepoMock = new Mock<IProducerRepository>();
        public static readonly Mock<IGenreRepository> genreRepoMock = new Mock<IGenreRepository>();

        public static readonly List<Movie> _movies = new()
        {
            new Movie()
            {
                Id = 1,
                Title = "up",
                Plot = "American",
                YearOfRelease = 2011,
                PosterLink = "poster-url",
                ProducerId = 1
            }
        };
        public static List<Actor> _actors = new()
        {
            new Actor
            {
                Id = 1,
                Name = "Christian Bale",
                Bio = "British",
                DateOfBirth = new DateTime(1979, 03, 02),
                Gender = (Gender)1
            },
            new Actor
            {
                Id = 2,
                Name = "Matt Damon",
                Bio = "American",
                DateOfBirth = new DateTime(1970, 10, 08),
                Gender = (Gender)1
            }
        };

        public static readonly List<Producer> _producers = new()
        {
            new Producer()
            {
                Id = 1,
                Name = "Christian Bale",
                Bio = "British",
                DateOfBirth = new DateTime(1979, 03, 02),
                Gender = (Gender)1
            },
            new Producer()
            {
                Id = 2,
                Name = "Mila Kunis",
                Bio = "Ukranian",
                DateOfBirth = new DateTime(1973, 06, 22),
                Gender = (Gender)2
            },
            new Producer
            {
                Id = 3,
                Name = "James Mangold",
                Bio = "American",
                DateOfBirth = new DateTime(1963, 12, 16),
                Gender = (Gender)1
            }
        };

        public static readonly List<Genre> _genres = new()
        {
            new Genre()
            {
                Id = 1,
                Name = "Action",

            },
            new Genre()
            {
                Id = 2,
                Name = "fun",

            }
        };

        public static void MockMovieRepo()
        {
            actorRepoMock.Setup(repo => repo.GetActorsByMovie(It.IsAny<int>())).Returns((int id) =>
            {
                return _actors;
            });

            producerRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return _producers.FirstOrDefault(x => x.Id == id);
            });

            genreRepoMock.Setup(repo => repo.GetGenresByMovie(It.IsAny<int>())).Returns(() =>
            {
                return _genres;
            });

            movieRepoMock.Setup(repo => repo.Add(It.IsAny<Movie>(), It.IsAny<string>(), It.IsAny<string>())).Returns(1);
            movieRepoMock.Setup(repo => repo.Update(It.IsAny<Movie>(), It.IsAny<string>(), It.IsAny<string>()));
            movieRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
            movieRepoMock.Setup(repo => repo.GetEntities()).Returns(() =>
            {
                return _movies;
            });

            movieRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) => {
                return _movies.FirstOrDefault(x => x.Id == id);
            });

            movieRepoMock.Setup(repo => repo.GetBy(It.IsAny<int>())).Returns((int yearOfRelease) =>
            {
                return _movies.Where(x => x.YearOfRelease == yearOfRelease);
            });
            actorRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) => {
                return _actors.FirstOrDefault(a => a.Id == id);
            });
            genreRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) => {
                return _genres.FirstOrDefault(a => a.Id == id);
            });
            movieRepoMock.Setup(repo => repo.GetEntities()).Returns(() =>
            {
                return _movies;
            });
        }
    }
}
