﻿using IMDB.Domain.Entity;
using IMDB.Repository.Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMDB.Test
{
    class ProducerMocks
    {
        public static readonly Mock<IProducerRepository> producerRepoMock = new Mock<IProducerRepository>();


        public static readonly List<Producer> _producers = new()
        {
            new Producer()
            {
                Id = 1,
                Name = "Christian Bale",
                Bio = "British",
                DateOfBirth = new DateTime(1979, 03, 02),
                Gender = (Gender)1
            },
            new Producer()
            {
                Id = 2,
                Name = "Mila Kunis",
                Bio = "Ukranian",
                DateOfBirth = new DateTime(1973, 06, 22),
                Gender = (Gender)2
            },
            new Producer
            {
                Id = 3,
                Name = "James Mangold",
                Bio = "American",
                DateOfBirth = new DateTime(1963, 12, 16),
                Gender = (Gender)1
            }
        };

        public static void MockProducerRepo()
        {
            producerRepoMock.Setup(repo => repo.GetEntities()).Returns(() =>
            {
                return _producers;
            });
            producerRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) =>
            {
                return _producers.SingleOrDefault(p => p.Id == id);
            });
            producerRepoMock.Setup(repo => repo.Add(It.IsAny<Producer>())).Returns(1);
            producerRepoMock.Setup(repo => repo.Update(It.IsAny<Producer>()));
            producerRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
    }
}
